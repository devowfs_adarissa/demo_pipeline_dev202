<?php

use Dev\Act101\compte;
use Dev\Act101\client;

use PHPUnit\Framework\TestCase;

class CompteTest extends TestCase
{
    /**
     *  @dataProvider dataForCrediter
     */
    public function testCrediter($somme, $solde1, $nvSolde1, $solde2, $nvSolde2)
    {
        $clt = new client(nom: "Alaoui");
        $compte1 = new compte($solde1, $clt);
        $compte2 = new compte($solde2);
        $compte1->créditer($somme, $compte2);
        $this->assertEquals($nvSolde1, $compte1->getSolde());
        $this->assertEquals($nvSolde2, $compte2->getSolde());
    }

    private function dataForCrediter()
    {
        return [
            [300, 5000, 5300, 4000, 3700],
            [0, 0, 0, 0, 0],
            [7000, 10000, 17000, 8000, 1000]
        ];
    }

    /**
     *  @dataProvider dataForDebiter
     */
    public function testDebiter($somme, $solde1, $nvSolde1, $solde2, $nvSolde2)
    {
        $clt = new client(nom: "Alaoui");
        $compte1 = new compte($solde1, $clt);
        $compte2 = new compte($solde2);
        $compte1->débiter($somme, $compte2);
        $this->assertEquals($nvSolde1, $compte1->getSolde());
        $this->assertEquals($nvSolde2, $compte2->getSolde());
    }

    private function dataForDebiter()
    {
        return [
            [300, 5000, 4700, 4000, 4300],
            [0, 0, 0, 0, 0],
            [7000, 10000, 3000, 8000, 15000]
        ];
    }
}
