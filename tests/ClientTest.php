<?php

use PHPUnit\Framework\TestCase;
use Dev\Act101\client;

class ClientTest extends TestCase
{
    /**
     * @dataProvider dataForSetCIN
     */
    public function testSetCin($cin)
    {
        $clt = new client();
        $this->expectException(Exception::class);
        $clt->setcin($cin);
    }
    public function dataForSetCIN()
    {
        // "/^[A-Z]{2,4}\d{2,7}$/"
        return [
            ["CD_63673"],
            ["3HEUYE"],
            ["R897397"],
            ["ytutzz"],
            ["ty3999"],
            ["__3999"],
        ];
    }

    public function testExperience()
    {
        $dateE = "12/09/2000";
        $c = new client(dateEmbauche: $dateE);
        $this->assertEquals(21, $c->experience());
        $this->assertEquals("09/12/2000", $c->getDateEmbauche()->format("d/m/Y"));
    }
    public function testGetNom()
    {
        $c = new client(nom: "ahmed");
        $this->assertEquals("AHMED", $c->getnom());
    }
}
